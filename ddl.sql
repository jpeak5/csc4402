use project;
DROP TABLE IF EXISTS orders;
DROP TABLE IF EXISTS price;
DROP TABLE IF EXISTS inventory;
DROP TABLE IF EXISTS vendor_itemcost;
DROP TABLE IF EXISTS vendors;
DROP TABLE IF EXISTS ingredients;

CREATE TABLE ingredients (
    type_ID int,
    Iname varchar(20),
    type varchar(20),
    Iqty float,
    PRIMARY KEY (type_ID)
);

CREATE TABLE orders (
    id          int NOT NULL AUTO_INCREMENT,
    date   datetime,
    size        int,
    coffee_type int,
    milk_type   int,
    flavor_type int,
    extra       int,
    PRIMARY KEY (id),
    foreign key (coffee_type) references ingredients(type_ID),
    foreign key (milk_type) references ingredients(type_ID),
    foreign key (flavor_type) references ingredients(type_ID),
--     foreign key (extra) references ingredients(type_ID)
);

CREATE TABLE price(
	type_ID int, 
	cost float,
	PRIMARY KEY (type_ID), 
	foreign key (type_ID) references ingredients(type_ID)
);

CREATE TABLE inventory (
	INV_ID int, 
	qty float, 
	min_qty int,
	PRIMARY key (INV_ID),
	foreign key (INV_ID) references ingredients(type_ID)
);

CREATE TABLE vendors (
    vendor_ID int, 
    Vname varchar(20), 
    address varchar(20),
    phone varchar(14),
    PRIMARY key (vendor_ID)
);

CREATE TABLE vendor_itemcost (
	vendor_ID int, 
	INV_ID int, 
	V_cost int,
	PRIMARY key (vendor_ID, INV_ID),
	foreign key (vendor_ID) references vendors(vendor_ID),
	foreign key (INV_ID) references ingredients(type_ID)
);


insert into ingredients values(1, 'Whole', 'Milk', 0.04);
insert into ingredients values(2, 'Fat-Free', 'Milk', 0.04);
insert into ingredients values(3, 'Soy', 'Milk', 0.04);
insert into ingredients values(4, 'Rice', 'Milk', 0.04);
insert into ingredients values(5, 'Almond', 'Milk', 0.04);
insert into ingredients values(6, 'Organic', 'Milk', 0.04);
insert into ingredients values(7, 'Vanilla', 'Flavor', 0.002);
insert into ingredients values(8, 'Blueberry', 'Flavor', 0.002);
insert into ingredients values(9, 'Coconut', 'Flavor', 0.002);
insert into ingredients values(10, 'Raspberry', 'Flavor', 0.002);
insert into ingredients values(11, 'Caramel', 'Flavor', 0.002);
insert into ingredients values(12, 'Hazelnut', 'Flavor', 0.002);
insert into ingredients values(13, 'Cinnamon', 'Flavor', 0.002);
insert into ingredients values(14, 'Light Roast', 'Coffee', 0.017);
insert into ingredients values(15, 'Medium Roast', 'Coffee', 0.017);
insert into ingredients values(16, 'Medium-Dark Roast', 'Coffee', 0.017);
insert into ingredients values(17, 'Dark Roast', 'Coffee', 0.017);
insert into ingredients values(18, NULL, 'Shot', 0.017);

insert into inventory values(1, 58, 15);
insert into inventory values(2, 54, 15);
insert into inventory values(3, 38, 15);
insert into inventory values(4, 56, 15);
insert into inventory values(5, 12, 15);
insert into inventory values(6, 8, 15);
insert into inventory values(7, 7, 4);
insert into inventory values(8, 8, 4);
insert into inventory values(9, 6, 4);
insert into inventory values(10, 7, 4);
insert into inventory values(11, 2, 4);
insert into inventory values(12, 7, 4);
insert into inventory values(13, 1, 4);
insert into inventory values(14, 150, 100);
insert into inventory values(15, 167, 100);
insert into inventory values(16, 76, 100);
insert into inventory values(17, 28, 100);

insert into price values(1, 0.5);
insert into price values(2, 0.5);
insert into price values(3, 0.5);
insert into price values(4, 0.8);
insert into price values(5, 0.8);
insert into price values(6, 0.8);
insert into price values(7, 0.7);
insert into price values(8, 0.7);
insert into price values(9, 0.7);
insert into price values(10, 0.7);
insert into price values(11, 0.7);
insert into price values(12, 0.7);
insert into price values(13, 0.7);
insert into price values(14, 3.2);
insert into price values(15, 3.5);
insert into price values(16, 3.6);
insert into price values(17, 3.8);
insert into price values(18, 0.8);

insert into vendors values(1, "Coffee Inc", "102 Walker Rd", "(225) 578-1234");
insert into vendors values(2, "Milk Inc", "105 Staton Rd", "(225) 578-1325");
insert into vendors values(3, "Flavor Inc", "112 Princeton Rd", "(225) 578-1879");

insert into vendor_itemcost values(2, 1, 3);
insert into vendor_itemcost values(2, 2, 3);
insert into vendor_itemcost values(2, 3, 3);
insert into vendor_itemcost values(2, 4, 4);
insert into vendor_itemcost values(2, 5, 5);
insert into vendor_itemcost values(2, 6, 5);
insert into vendor_itemcost values(3, 7, 18);
insert into vendor_itemcost values(3, 8, 19);
insert into vendor_itemcost values(3, 9, 17);
insert into vendor_itemcost values(3, 10, 17);
insert into vendor_itemcost values(3, 11, 18);
insert into vendor_itemcost values(3, 12, 19);
insert into vendor_itemcost values(3, 13, 18);
insert into vendor_itemcost values(1, 14, 11);
insert into vendor_itemcost values(1, 15, 12);
insert into vendor_itemcost values(1, 16, 11);
insert into vendor_itemcost values(1, 17, 12);
