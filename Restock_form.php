<?php
require_once 'Size.php';

class Restock_form {
    public static function getString(){
        $type       = Ingredient::fetchAll();
        $formElements[] = new Node('h3', array(), 'Type');
        $formElements[] = SQLUtil::getSelect(array('key'=>'type_ID', 'value'=>'Iname'), $type, array('name'=>'type', 'id'=>'type'));
		$qty        = SQLUtil::getQtyById('Type');


        $formElements[] = new Node('h3', array(), 'Restock amount');
        $formElements[] = new Node('input', array('type'=>'text', 'id'=>'bought', 'name'=>'bought', 'size'=>1, 'value'=>0));
        
        $formElements[] = new Node('br', array(), '');
        $formElements[] = new Node('input', array('type'=>'submit','name'=>'submit', 'value'=>'submit'));

        $form = new Node('form', array('method'=>'post'), $formElements);

        return $form->toString();

    }
   
    
}

?>
