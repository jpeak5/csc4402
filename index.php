<?php
require_once 'Layout.php';
require_once 'Node.php';
require_once 'Database.php';
require_once 'Table.php';
global $db;     //global db connection

Layout::header();

$header  = new Node('h1', array('class'=>'title'), 'CSC 4402 Coffees');

$posH1   = new Node('h2', array(), 'Inventory');
$plistH1 = new Node('h2', array(), 'POS');
$salesH1 = new Node('h2', array(), 'Sales Report');
$stockH1 = new Node('h2', array(), 'Restock Items');

$priceList          = new Node('a', array('href'=>'inventory.php'), $posH1);
$pos                = new Node('a', array('href'=>'POS.php'), $plistH1);
$sales              = new Node('a', array('href'=>'sales.php'), $salesH1);
$restock             = new Node('a', array('href'=>'Restock.php'), $stockH1);

$elements = array($header, $pos, $priceList, $sales, $restock);
$content = new Node('div', array('class'=>'content'),$elements);
//echo $header->toString();
//echo $pos->toString();
//echo $priceList->toString();
echo $content->toString();



Layout::footer();
?>
