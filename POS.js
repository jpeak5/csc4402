function getPrice(id){
    var prices = $("#price").data('price');

//    console.log("get price: " + prices[id]);
    return prices[id];
}

function calculateTotal(){
    var $coffeetype = $("#coffeetype :selected");
    var $flavortype = $("#flavortype :selected");
    var $milktype   = $("#milktype :selected");
    var $size       = $("#size :selected");
    var $extra      = $("#extra");
    var $extraCost  = $extra.val() * 0.8;
    console.log("extra value is " + $extra.val());

    var multi       = $size.attr("value");
    console.log("size is " + multi);
    
    var coffeecost  = getPrice($coffeetype.attr("value")) * multi + $extraCost;
    console.log("coffee cost is " + coffeecost);
    
    
    var flavorprice = getPrice($flavortype.attr("value"));
    if(typeof flavorprice == 'undefined'){
        flavorprice = 0;
    }
    var flavorcost  = flavorprice * multi;
    console.log("flavor cost is " + flavorcost);
    
    var milkcost    = getPrice($milktype.attr("value")) * multi;
    console.log("milk cost is " + milkcost);

    var total = coffeecost + flavorcost + milkcost;

    return total.toFixed(2);
}

function displayTotal() {
    var total = calculateTotal();
    $('#pos-total').html("$" + total);
}


$("#flavortype").prop("selectedIndex", -1)
displayTotal();



$('select').on('change', function(){
    displayTotal();
});

$('input').on('change', function(){
    displayTotal();
});