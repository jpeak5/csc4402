<?php

class Node {    
    public $name;
    public $attributes;
    public $data;
    
    public function __construct($tagname, array $attrs = array(), $data=''){
        $this->name       = $tagname;
        $this->attributes = $attrs;
        $this->data       = $data;
//        var_dump($this);
    }
    
    public function getValue(){
//                        echo 'got here';
//                        var_dump($this);
        $doc = new DOMDocument();
        
        if(is_string($this->data) or is_numeric($this->data) or is_null($this->data)){
//            echo sprintf("<pre>create text node with name %s</pre>", $this->name);
            $node = $doc->createElement($this->name, htmlentities($this->data));
            $doc->appendChild($node);
            
        }elseif(is_array($this->data)){
        
//            echo sprintf("<pre>create compound node with name %s; child count = %d</pre>", $this->name, count($this->data));
            $node = $doc->createElement($this->name);
            foreach($this->data as $d){
//                echo 'got here';
                $newNode = $doc->importNode($d->getValue(), true);
                $node->appendChild($newNode);
            }
//            return $domNode;
            
        }elseif(is_object($this->data)){ //object
            
//            echo sprintf("<pre>create new node with name %s</pre>", $this->name);
            $node = $doc->createElement($this->name);
            $node->appendChild($doc->importNode($this->data->getValue(), true));
//            echo sprintf("type of node = %s", get_class($node));
            
        }else{
            var_dump($this);
            echo "unhandled case";
        }
        
        foreach($this->attributes as $name=>$value){
            $node->setAttribute($name, htmlentities($value));
        }
        
        return $node;
    }
    
    public function toString(){
        $doc = new DOMDocument();
        $value = $doc->importNode($this->getValue(), true);

        $doc->appendChild($value);
        return html_entity_decode($doc->saveHTML($doc));
    }
}

class SelectNode extends Node {
    public function __construct(array $attributes = array(), array $options = array(), $selected=null) {
        
        $is_selected = function($v) use($selected){
            if(!isset($selected)){
                return false;
            }elseif(is_array($selected)){
                return in_array($v, $selected);
            }else{
                return $v == $selected;
            }
        };
        
        $nodeOptions = array();
        foreach($options as $o=>$attrs){
            if($is_selected($attrs['value'])){
                $attrs['selected'] = 'selected';
            }
            $nodeOptions[] = new Node('option', $attrs, $o);
        }
        return parent::__construct('select', $attributes, $nodeOptions);
    }
}

class Tag {
    
    public static function Write($name,array $attr = array(), $value=null){
        $node = new Node($name, $attr, $value);
        return $node->toString();
    }
}
?>
