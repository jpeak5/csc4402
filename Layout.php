<?php

class Layout {
    public static function header(){
        $header = "
            <html>
                <head>
                    <link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\">
                </head>
                <body>
                <a href=\"index.php\">Home</a>";
        echo $header;
    }
    
    public static function footer($script = null){
        //close db connection
        global $db;
        $db = null;
        
        $footer = "
                    <script src=\"//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js\"></script>
                    <script src=\"{$script}\"></script>
                </body>
            </html>";
        echo $footer;
    }
}
?>
