<?php
require_once 'Table.php';
require_once 'Node.php';
require_once 'Layout.php';

Layout::header();

$itemPrices = SQLUtil::inventory();

$columns = array(
    'id',
    'type',
    'name',
    'cost per 8oz drink',
    'qty used per 8oz drink',
    'qty on hand',
    'min qty required',
    );


$theads = array();
foreach($columns as $c){
    $theads[] = new Node('th', array(), $c);
}
$tableHeaderRow = new Node('tr', array(), $theads);

$rows = array($tableHeaderRow);
$i=0;
foreach($itemPrices as $ip){

    $row    = array();
    $rowAttrs = array();
    
    $row[]  = new Node('td', array(), $ip->type_ID);
    $row[]  = new Node('td', array(), $ip->type);
    $row[]  = new Node('td', array(), $ip->Iname);
    $row[]  = new Node('td', array('class'=>'align-right'), sprintf('$%s',$ip->cost));
    $row[]  = new Node('td', array('class'=>'align-right'), $ip->Iqty);
    $row[]  = new Node('td', array('class'=>'align-right'), $ip->qty);
    $row[]  = new Node('td', array('class'=>'align-right'), $ip->min_qty);
    
    //highligh low inventory
    if($ip->qty < $ip->min_qty){
        $rowAttrs += array('class'=>'inventory-too-low');
    }
    
    //zebra
    if($i%2 ==0){
        $current = '';
        if(isset($rowAttrs['class'])){
            $current = $rowAttrs['class'];
        }
        $rowAttrs['class'] = $current." zebra";
    }
    
    $rows[] = new Node('tr', $rowAttrs, $row);
    $i++;
}

$table      = new Node('table', array(), $rows);
$header = new Node('h1', array(), 'Inventory Overview');

echo $header->toString();
echo $table->toString();

Layout::footer();
?>
