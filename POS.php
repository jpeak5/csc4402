<?php
require_once 'Layout.php';
require_once 'Node.php';
require_once 'Table.php';
require_once 'POS_form.php';
require_once 'Size.php';

Layout::header();

//wrap incoming params into obj or false
if(isset($_POST['submit'])){
    $post = new stdClass();
    foreach($_POST as $pkey => $pvalue){
        $post->$pkey = $pvalue;
    }
}else{
    $post = false;
}

    
if($post){
    $coffee = Ingredient::fetchBy(array('type_ID'=>$post->coffeetype));
    $flavor = isset($post->flavortype) ? Ingredient::fetchBy(array('type_ID'=>$post->flavortype)) : false;
    $milk   = Ingredient::fetchBy(array('type_ID'=>$post->milktype));
    $size   = Size::getSizeById($post->size);
    $time   = strftime('%F %T', time());
    
    $extraCoffeeConsumed = $post->extra * $coffee->Iqty;

    
    
    $coffee->consumed = ($coffee->Iqty * $size->multiplier) + $extraCoffeeConsumed;

    if($flavor){
        $flavor->consumed = $flavor->Iqty * $size->multiplier;
    }
    $milk->consumed   = $milk->Iqty * $size->multiplier;
    
    $order = new Order();
    $order->date = $time;
    $order->size = $size->multiplier;
    $order->coffee_type = $coffee->type_ID;
    $order->flavor_type = $flavor != false ? $flavor->type_ID : null;
    $order->milk_type = $milk->type_ID;
    $order->extra = $post->extra;
    if(Order::insert($order)){
//        echo "insert on Orders table successful";
    }else{
        echo "insert on Orders table FAIL";
        die();
    }
    
    //update inventory table
    
    if($flavor){
        Inventory::update($flavor);
    }
    Inventory::update($coffee);
    Inventory::update($milk);
    header('Location: POS.php');
}


$total = new Node('h2', array('id'=>'pos-total'), '');



echo POS_form::getString();
echo $total->toString();

Layout::footer('POS.js');
?>
