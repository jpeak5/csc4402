<?php
require_once 'Size.php';

class POS_form {
    public static function getString(){
        
        $formElements[] = new Node('h3', array(), 'Size');
        $formElements[] = POS_form::getSizesSelect();
        
        $coffees        = Ingredient::fetchBy(array('type'=>'Coffee'));
        $milk           = Ingredient::fetchBy(array('type'=>'Milk'));
        $flavors        = Ingredient::fetchBy(array('type'=>'Flavor'));
        
        $ingredients = Ingredient::fetchAll();
        $keys = array();
        foreach($ingredients as $i){
            $keys[] = (int)$i->type_ID;
        }
        $prices = Price::fetchByIn('type_ID', $keys);
        $priceData = array();
        foreach($prices as $price){
            $priceData[$price->type_ID] = $price->cost;
        }
        
        $jsonPriceData = json_encode($priceData);
        $priceDataset = new Node('div', array('data-price'=>$jsonPriceData, 'id'=>'price'));
        echo $priceDataset->toString();

        $formElements[] = new Node('h3', array(), 'Coffee');
        $formElements[] = SQLUtil::getSelect(array('key'=>'type_ID', 'value'=>'Iname'), $coffees, array('name'=>'coffeetype', 'id'=>'coffeetype'));

        $formElements[] = new Node('h3', array(), 'Flavor');
        $formElements[] = SQLUtil::getSelect(array('key'=>'type_ID', 'value'=>'Iname'), $flavors, array('name'=>'flavortype', 'id'=>'flavortype'));

        
        $formElements[] = new Node('h3', array(), 'Milk');
        $formElements[] = SQLUtil::getSelect(array('key'=>'type_ID', 'value'=>'Iname'), $milk, array('name'=>'milktype', 'id'=>'milktype'));

        $formElements[] = new Node('h3', array(), '#Extra Shots');
        $formElements[] = new Node('input', array('type'=>'text', 'id'=>'extra', 'name'=>'extra', 'size'=>1, 'value'=>0));
        
        $formElements[] = new Node('br', array(), '');
        $formElements[] = new Node('input', array('type'=>'submit','name'=>'submit', 'value'=>'submit'));

        $form = new Node('form', array('method'=>'post'), $formElements);

        return $form->toString();

    }
    
    private static function getSizesSelect(){
        global $size;
        $sizes = array($size->small, $size->medium, $size->large);
        
        $attrs = array('id'=>'size', 'name'=>'size');
        
        return SQLUtil::getSelect(array('key'=>'multiplier', 'value'=>'name'), $sizes, $attrs);
        
    }
    
}

?>
