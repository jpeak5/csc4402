<?php
require_once 'Database.php';

class Table {
    
    public static $fields;
    public static $tablename;

    public static function instantiate($params){
        $obj = new static;
        foreach($params as $k=>$v){
            if(in_array($k, static::$fields)){
                $obj->$k = $v;
            }
        }
        return $obj;
    }
    

    public static function fetchAll(){
        global $db;
        $table  = static::$tablename;
        
        $qry    = $db->prepare("Select * FROM {$table}");$qry->execute();
        
        $rows   = array();
        
        if($qry->execute()){
            while($row = $qry->fetch()){
                $rows[] = static::instantiate($row);
            }
        }
        
        return $rows;
    }
    
    /**
     * 
     * @param array $params associative array of params needed in the 
     * WHERE clause of the query; these follow the pattern field=>value
     */
    public static function fetchBy($params){
        global $db;
        $where = 'WHERE';
        foreach($params as $field=>$value){
            $where .= sprintf(" %s = '%s'", $field, $value);
        }
        $table  = static::$tablename;
        $sql    = sprintf("SELECT * FROM %s %s", $table, $where);

        $result = $db->prepare($sql);
        $rows   = array();
        if($result->execute()){
            while($row = $result->fetch()){
                $rows[]  = (object)$row;
            }
        }
        return count($rows) == 1 ? array_pop($rows) :$rows;
    }
    
    public static function fetchByIn($field, array $set){
        global $db;
        $table  = static::$tablename;
        $setValues = implode(',',$set);
        
//        $sql    = sprintf("SELECT * FROM %s WHERE :field IN (:set)", $table);
        $sql    = sprintf("SELECT * FROM %s WHERE type_ID IN (%s)", $table, $setValues);
        $stmt   = $db->prepare($sql);
        $stmt->bindParam(':field', $field);
        $stmt->bindParam(':set', $setValues);
//        $stmt->debugDumpParams();
        $rows = array();
        if($stmt->execute()){
            while($row = $stmt->fetch()){
                $rows[] = (object)$row;
            }
            return $rows;
        }else{
            return false;
        }
        
    }
    
    /**
     * Inserts a row into the DB;
     * @global type $db
     * @param Table $ing
     */
    public static function insert(Table $ing){
        global $db;
        $fieldsToInsert = array_diff(static::$fields, static::$keyfields);
        $fieldString = sprintf("%s",implode(',', $fieldsToInsert));
        
        
        $values = array();
        foreach($fieldsToInsert as $field){
            if($ing->$field == ''){
                $values[] = null;
            }else{
                $values[] = "'".$ing->$field."'";
            }
        }
        
        $sql = sprintf("INSERT INTO %s (%s) VALUES (%s)",static::$tablename,$fieldString, implode(',', $values));
        
        $stmt = $db->prepare($sql);
        $stmt->debugDumpParams();

        return $stmt->execute();
    }
    
    public function form_text($attribute, $type='text') {
        $value = isset($this->$attribute) ? htmlentities($this->$attribute) : "";
        return new Node('input', array('id'=>$attribute, 'type'=>$type, 'name'=>$attribute, 'value'=>$value));
    }
    
    public function form_hidden($attribute) {
        return $this->form_text($attribute,'hidden');
    }
    
}

class Ingredient extends Table{
    public static $fields    = array('type_ID', 'Iname', 'type');
    public static $tablename = 'ingredients';
    public $consumed;
	public $bought;
}

class Price extends Table {
    public static $fields    = array('type_ID', 'cost');
    public static $tablename = 'price';
}

class Order extends Table {
    public static $fields = array('id', 'date', 'size', 'coffee_type', 'milk_type', 'flavor_type', 'extra');
    public static $tablename = 'orders';
    public static $keyfields = array('id');
    
    public static function insert(Table $order){
        global $db;
        $vars = get_object_vars($order);
        
        $fieldStr = implode(',',array_keys($vars));
        foreach($vars as $k => $v){
            if(empty($v)){
                echo 'got here';
                $vars[$k] = 'NULL';
            }else{
                $vars[$k] = "'".$v."'";
            }
        }
        $valStr   = implode(',',array_values($vars));

        $sql = sprintf("INSERT INTO %s (%s) VALUES (%s)", self::$tablename, $fieldStr, $valStr);

        $stmt = $db->prepare($sql);

        $success = $stmt->execute();
        if(!$success){
            $stmt->debugDumpParams();
            var_dump($stmt->errorInfo());
        }
        return $success;
    }
}

class Inventory extends Table{
    public static $fields = array('INV_ID', 'qty', 'min_qty');
    public static $tablename = 'inventory';
    
    public static function update($item){
        global $db;
        $oldRecord = Inventory::fetchBy(array('INV_ID'=>$item->type_ID));
        
        $deduction = isset($item->consumed) ? (float)$item->consumed : 0;
        $qty       = (float)$oldRecord->qty - $deduction;
        
        $sql = sprintf("UPDATE %s SET qty = :qty WHERE INV_ID = :inv_id", self::$tablename);
        
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':qty', $qty);
        $stmt->bindParam(':inv_id', $item->type_ID);
        
        $stmt->execute();
    } 
	public static function updated($item){
        global $db;
        $oldRecord = Inventory::fetchBy(array('INV_ID'=>$item->INV_ID));
        //print_r(($oldRecord));
        $addition = isset ($item->bought) ? (int)$item->bought: 0;
		//print_r($oldRecord->qty);
		//print_r($item->bought);
		 //print_r($addition);
        $qty       = (float)$oldRecord->qty + $addition;

        $sql = sprintf("UPDATE %s SET qty = :qty WHERE INV_ID = :inv_id", self::$tablename);
        
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':qty', $qty);
        $stmt->bindParam(':inv_id', $item->INV_ID);
        
//        echo "insert on Inventory table successful";
        return $stmt->execute();

//		print_r("\n" + "New total number is" + $qty);
    }
	
	
}

class SQLUtil {
    
    public static function inventory(){
        global $db;
        $qry = $db->prepare("
            select *
            from ingredients
                natural join price
                    join inventory
            where type_ID=INV_ID"
                );
        
        $rows = array();
        if($qry->execute()){
            while($row = $qry->fetch()){
                $rows[] = (object)$row;
            }
        }
        return $rows;
    }
    
    
    /**
     * 
     * @param array $guide an associative array containing the key
     * and value attribute for the particular type passed in the second param
     * array('key'=>'mykeyfield', 'value'=>'myvaluefield')
     * @param array $items an array of objects to use in the select list
     * @param attrs html attributes to be included
     * @param string $selected key that should be selected
     * @param bool $none whether or not to insert an entry for NONE/NULL
     * @return \SelectNode
     */
    public static function getSelect(array $guide, array $items, array $attrs= array(), $selected=null, $none = false){
        $key        = $guide['key'];
        $value      = $guide['value'];
        $options    = array();
        
        foreach($items as $item){
            $options[$item->$value] = array('value'=>$item->$key);
        }
        
        return new SelectNode($attrs, $options, $selected);
    }
    

    /**
     * 
     * @param int $id type_ID of the item
     */
    public static function itemPrice($id){
        global $db;
        $sql = sprintf("SELECT cost from price WHERE type_ID = :id");
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':id', $id);
        
        if($stmt->execute()){
            $rows = array();
            while($row = $stmt->fetch()){
                $rows[] = $row;
            }
            assert(count($rows)==1);
            return array_pop($rows);
        }else{
            return false;
        }
    }
    
    public static function salesByType($type){
        global $db;
        $sql = sprintf("
            SELECT 
                i.type,
                i.Iname,
                o.size, 
                o.%s_type, 
                p.cost, 
                i.Iqty, 
                p.cost*o.size AS revenue, 
                o.extra
            FROM orders o 
                INNER JOIN ingredients i on o.%s_type = i.type_ID
                INNER JOIN price p ON p.type_ID = o.%s_type;
                ", $type, $type, $type);

        $stmt = $db->prepare($sql);
        if($stmt->execute()){
            $rows = array();
            while($row = $stmt->fetch()){
                $rows[] = (object)$row;
            }
            return $rows;
        }else{
            return false;
        }
    }
    
    public static function extraShotPrice(){
        global $db;
        $sql = "select * from ingredients natural join price where type = 'shot';";
        $stmt = $db->prepare($sql);
        
        if($stmt->execute()){
            $rows = array();
            while($row = $stmt->fetch()){
                $rows[] = (object)$row;
            }
            return array_pop($rows);
        }else{
            return false;
        }
        
    }
	
	public static function getQtyById($type_ID){
        global $db;
        $sql = "select Iname, qty from inventory, ingredients where type_id = INV_ID and type_ID = :type_ID;";
        $stmt = $db->prepare($sql);
         $stmt->bindParam(':type_ID', $type_ID);
        $stmt->execute();
        
    }
}

?>
