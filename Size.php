<?php
class Size {
    
    public $id;
    public $name;
    public $multiplier;
    public $ounces;
    
    
    public function __construct($id, $name, $multiplier, $oz){
        $this->id = $id;
        $this->name = $name;
        $this->multiplier = $multiplier;
        $this->ounces = $oz;
    }
    
    /**
     * 
     * @param int $mult key to Size object in $size global
     */
    public static function getSizeById($mult){
        global $size;
        $sizes = array($size->small, $size->medium, $size->large);
        
        foreach($sizes as $size){
            if($size->multiplier == $mult){
                return $size;
            }
        }
        return false;
    }
}

$size = new stdClass();
$size->small  = new Size('sm','Small',1, 8);
$size->medium = new Size('med','Medium',2, 16);
$size->large  = new Size('lg','Large',3, 24);

?>
