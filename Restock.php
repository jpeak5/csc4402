<?php
require_once 'Layout.php';
require_once 'Node.php';
require_once 'Table.php';
require_once 'Restock_form.php';

Layout::header();

//wrap incoming params into obj or false
if(isset($_POST['submit'])){
    $post = new stdClass();
    foreach($_POST as $pkey => $pvalue){
        $post->$pkey = $pvalue;
    }
}else{
    $post = false;
}

    
if($post){
    $type = Inventory::fetchBy(array('INV_ID'=>$post->type));
    $qty   = SQLUtil::getQtyById('Type');
    $type->bought = $post->bought;

    $inventory = new Inventory();
    $inventory->qty = $post->bought;

     
    
    //update inventory table
    if(Inventory::updated($type)){
        echo "Inventory has been updated successful";
    }else{
        die('error updating Inventory table');
    }

    header('Location: Restock.php');
}


$total = new Node('h2', array('id'=>'stock-total'), '');



echo Restock_form::getString();
echo $total->toString();

Layout::footer('Restock.js');
?>
