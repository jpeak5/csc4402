<?php
require_once 'Table.php';
require_once 'Node.php';
require_once 'Layout.php';

Layout::header();

$coffee = SQLUtil::salesByType('coffee');
$flavor = SQLUtil::salesByType('flavor');
$milk   = SQLUtil::salesByType('milk');
$xshot  = SQLUtil::extraShotPrice();

foreach($coffee as $c){
    $c->revenue += $c->extra*$xshot->cost;
}

$data = array_merge($coffee, $flavor, $milk);

$columns = array('type', 'sub-type', 'revenue');

$theads = array();
foreach($columns as $c){
    $theads[] = new Node('th', array(), $c);
}
$tableHeaderRow = new Node('tr', array(), $theads);
$rows = array($tableHeaderRow);
$i=0;
$aggregate = array();
foreach($data as $d){

    if(!isset($aggregate[$d->type])){
        $aggregate[$d->type] = array();
    }
    
    if(!isset($aggregate[$d->type][$d->Iname])){
        $aggregate[$d->type][$d->Iname] = 0;
    }
    
    $aggregate[$d->type][$d->Iname] += $d->revenue;
}

foreach($aggregate as $type=>$subtypes){

    foreach($subtypes as $subtype => $revenue){

        $row    = array();
        $rowAttrs = array();

        $row[]  = new Node('td', array(), $type);
        $row[]  = new Node('td', array(), $subtype);
        $row[]  = new Node('td', array('class'=>'align-right'), sprintf('$%01.2f',$revenue));


        //zebra
        if($i%2 ==0){
            $current = '';
            if(isset($rowAttrs['class'])){
                $current = $rowAttrs['class'];
            }
            $rowAttrs['class'] = $current." zebra";
        }

        $rows[] = new Node('tr', $rowAttrs, $row);
        $i++;
    }
}

$header     = new Node('h1', array(), 'Sales Report');
$table      = new Node('table', array(), $rows);

echo $header->toString();
echo $table->toString();

Layout::footer();
?>
